$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Login.feature");
formatter.feature({
  "line": 1,
  "name": "Testing Login Action",
  "description": "",
  "id": "testing-login-action",
  "keyword": "Feature"
});
formatter.before({
  "duration": 5090907772,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I navigate to login page",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginStep.iNavigateToHomePage()"
});
formatter.result({
  "duration": 141140540,
  "status": "passed"
});
formatter.scenario({
  "line": 6,
  "name": "Login with correct username and password",
  "description": "",
  "id": "testing-login-action;login-with-correct-username-and-password",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 7,
  "name": "I enter email as \"cagatayhan@yandex.com\" and password as \"1234Qwe!\"",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I should successfully logged in",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "cagatayhan@yandex.com",
      "offset": 18
    },
    {
      "val": "1234Qwe!",
      "offset": 58
    }
  ],
  "location": "LoginStep.iEnterEmailAsAndPasswordAs(String,String)"
});
formatter.result({
  "duration": 5556727278,
  "status": "passed"
});
formatter.match({
  "location": "LoginStep.iShouldSuccessfullyLoggedIn()"
});
formatter.result({
  "duration": 9272751,
  "status": "passed"
});
formatter.after({
  "duration": 959374490,
  "status": "passed"
});
formatter.before({
  "duration": 4548848874,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I navigate to login page",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginStep.iNavigateToHomePage()"
});
formatter.result({
  "duration": 368640,
  "status": "passed"
});
formatter.scenario({
  "line": 10,
  "name": "Login with incorrect username and password",
  "description": "",
  "id": "testing-login-action;login-with-incorrect-username-and-password",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 11,
  "name": "I enter email as \"cagatayhan@yandex.com\" and password as \"1234Qwe!\"",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "I should see error message",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "cagatayhan@yandex.com",
      "offset": 18
    },
    {
      "val": "1234Qwe!",
      "offset": 58
    }
  ],
  "location": "LoginStep.iEnterEmailAsAndPasswordAs(String,String)"
});
formatter.result({
  "duration": 5556085144,
  "status": "passed"
});
formatter.match({
  "location": "LoginStep.iShouldSeeErrorMessage()"
});
formatter.result({
  "duration": 30045607273,
  "error_message": "java.lang.AssertionError: expected [true] but found [false]\r\n\tat org.testng.Assert.fail(Assert.java:94)\r\n\tat org.testng.Assert.failNotEquals(Assert.java:513)\r\n\tat org.testng.Assert.assertTrue(Assert.java:42)\r\n\tat org.testng.Assert.assertTrue(Assert.java:52)\r\n\tat page.LoginPage.errorMessageIsDisplayed(LoginPage.java:45)\r\n\tat steps.login.LoginStep.iShouldSeeErrorMessage(LoginStep.java:60)\r\n\tat ✽.Then I should see error message(Login.feature:12)\r\n",
  "status": "failed"
});
formatter.embedding("image/png", "embedded0.png");
formatter.after({
  "duration": 1090361638,
  "status": "passed"
});
});
package steps.login;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import page.HomePage;
import page.LoginPage;
import runners.LoginRunner;
import utils.DriverFactory;
import utils.DriverManager;

/*
 * Created by cagatay.han on 2.06.2017.
 */
public class LoginStep extends LoginRunner {
    private HomePage homePage;
    private LoginPage loginPage;

    @Before
    public void setUp() {
        try {
            driverFactory = new DriverFactory();
            driverFactory.setUp(globalIpAddress, globalBrowser, globalEnvironment);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown(Scenario scenario) {
        if (scenario.isFailed()) {
            final byte[] screenshot = ((TakesScreenshot) DriverManager.getDriver()).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
        }
        driverFactory.tearDown();
    }

    @Given("^I navigate to login page$")
    public void iNavigateToHomePage() throws Throwable {
        loginPage = new LoginPage(DriverManager.getDriver());
    }

    @And("^I enter email as \"([^\"]*)\" and password as \"([^\"]*)\"$")
    public void iEnterEmailAsAndPasswordAs(String email, String password) throws Throwable {
        homePage = loginPage.tryLogin(email, password);
    }

    @Then("^I should successfully logged in$")
    public void iShouldSuccessfullyLoggedIn() throws Throwable {
        homePage.verifyLoggedIn();
    }

    @Then("^I should see error message$")
    public void iShouldSeeErrorMessage() throws Throwable {
        loginPage.errorMessageIsDisplayed();
    }
}

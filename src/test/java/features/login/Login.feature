Feature: Testing Login Action

  Background:
    Given I navigate to login page

  Scenario: Login with correct username and password
    When I enter email as "cagatayhan@yandex.com" and password as "1234Qwe!"
    Then I should successfully logged in

  Scenario: Login with incorrect username and password
    When I enter email as "cagatayhan@yandex.com" and password as "1234Qwe!"
    Then I should see error message
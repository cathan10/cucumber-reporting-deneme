package runners;

import cucumber.api.CucumberOptions;

/*
 * Created by cagatay.han on 5.06.2017.
 */
@CucumberOptions(
    features = {"src/test/java/features/boutique"},
    glue = "steps.boutique",
    format = {"json:target/cucumber-butik.json", "html:target/site/cucumber-pretty"}
)
public class BoutiqueRunner extends BaseRunner{
}

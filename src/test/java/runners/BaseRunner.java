package runners;

import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import utils.DriverFactory;
import utils.DriverManager;

/*
 * Created by cagatay.han on 5.06.2017.
 */
public class BaseRunner extends AbstractTestNGCucumberTests{
    public static DriverFactory driverFactory;
    public static String globalIpAddress = "";
    public static String globalBrowser = "";
    public static String globalEnvironment = "";


    @Parameters({"ipAddress", "browser", "environment"})
    @BeforeSuite
    public void beforeSuite(@Optional("") String ipAddress, @Optional("chrome") String browser, @Optional("dev") String environment) {
        globalIpAddress = ipAddress;
        globalBrowser = browser;
        globalEnvironment = environment;
    }

    @AfterSuite
    public void afterSuite() {
        if (DriverManager.getDriver() != null) {
            driverFactory.tearDown();
        }
    }
}

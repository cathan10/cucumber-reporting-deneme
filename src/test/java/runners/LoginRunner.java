package runners;

import cucumber.api.CucumberOptions;

/*
 * Created by cagatay.han on 2.06.2017.
 */
@CucumberOptions(
    features = {"src/test/java/features/login"},
    glue = "steps.login",
    format = {"json:target/cucumber-login.json", "html:target/site/cucumber-pretty"}
)
public class LoginRunner extends BaseRunner {

}

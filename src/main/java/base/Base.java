package base;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/*
 * Created by cagatay.han on 2.06.2017.
 */
public abstract class Base {
    protected static WebDriver driver;

    public Base(WebDriver driver) {
        this.driver = driver;
    }

    protected String getUrl() {
        return driver.getCurrentUrl();
    }

    protected String getTitle() {
        return driver.getTitle();
    }

    protected String getText(WebElement element) {
        return element.getText();
    }

    protected void refresh() {
        driver.navigate().refresh();
    }

    protected void navigate(String url) {
        driver.get(url);
    }

    protected void sleep(int second) {
        try {
            Thread.sleep(second*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void click(WebElement element) {
        element.click();
    }

    protected void fill(WebElement element, String text) {
        element.sendKeys(text);
    }

    protected void clearAndFill(WebElement element, String text) {
        element.clear();
        element.sendKeys(text);
    }

    protected void moveToElement(WebElement element) {
        Actions actions = new Actions(driver);
        highlightElement(element);
        actions.moveToElement(element);
        actions.perform();
    }

    protected void moveToElementJS(WebElement element) {
        highlightElement(element);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    private static void highlightElement(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
                " border: 2px solid red;");
    }

    private void highlightElementClick(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
                " border: 2px solid blue;");
    }

    protected void waitForPageLoad() {
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        sleep(2);
    }

    protected void waitForAjax() {
        while (true)
        {
            Boolean ajaxIsComplete = (Boolean) ((JavascriptExecutor)driver).executeScript("return jQuery.active == 0");
            if (ajaxIsComplete){
                break;
            }
        }
        sleep(1);
    }

    protected void waitUntilVisible(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    protected boolean isElementPresentAndDisplay(WebElement element) {
        try {
            return element.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }
 }

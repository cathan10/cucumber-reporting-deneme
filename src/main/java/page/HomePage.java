package page;

import base.Base;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

/*
 * Created by cagatay.han on 2.06.2017.
 */
public class HomePage extends Base {
    public HomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.LINK_TEXT, using = "Giriş Yap")
    private WebElement loginButton;
    @FindBy(how = How.CSS, using = ".usernamecontrol")
    private WebElement username;

    public LoginPage clickLoginButton() {
        waitForPageLoad();
        sleep(1);
        click(loginButton);
        waitForAjax();

        return new LoginPage(driver);
    }

    public void verifyLoggedIn() {
        Assert.assertTrue(getUrl().contains("orders"));
    }
}

package page;

import base.Base;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

/*
 * Created by cagatay.han on 5.06.2017.
*/
public class LoginPage extends Base{
    public LoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.CSS, using = "form>div:first-child>input")
    private WebElement usernameTextBox;
    @FindBy(how = How.CSS, using = "form>div:last-of-type>input")
    private WebElement passwordTextBox;
    @FindBy(how = How.CSS, using = "button")
    private WebElement submitButton;
    @FindBy(how = How.LINK_TEXT, using = "Şifremi unuttum?")
    private WebElement forgotPasswordLink;
    @FindBy (how = How.CSS, using = ".toast.toast-error")
    private List<WebElement> errorToast;

    public HomePage tryLogin(String email, String password) {
        waitForPageLoad();
        fill(usernameTextBox, email);
        fill(passwordTextBox, password);
        click(submitButton);
        waitForPageLoad();
        waitForAjax();

        return new HomePage(driver);
    }

    public void errorMessageIsDisplayed() {
        Assert.assertTrue(errorToast.size() > 0);
    }
}

package utils;

import java.io.FileInputStream;
import java.util.Properties;

/*
 * Created by cagatay.han on 2.06.2017.
 */
public class ConfigurationInstance {
    private static ConfigurationInstance instance = null;
    private Properties configFileProperties = new Properties();

    public static ConfigurationInstance getInstance(String environment) {
        if (instance == null) {
            instance = new ConfigurationInstance();
            instance.loadConfigProperties(environment);
        }
        return instance;
    }

    public String getConfigProperty(String key) {
        return this.configFileProperties.getProperty(key, "");
    }

    private void loadConfigProperties(String environment) {
        String configFileResource = System.getProperty("user.dir") + "/src/main/resources/config/" + environment + "/config.properties";
        try {
            FileInputStream input = new FileInputStream(configFileResource);
            this.configFileProperties.load(input);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

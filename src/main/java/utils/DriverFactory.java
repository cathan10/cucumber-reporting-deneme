package utils;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.*;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static utils.DriverManager.getDriver;
import static utils.DriverManager.setWebDriver;

/*
 * Created by cagatay.han on 2.06.2017.
 */
public class DriverFactory {
    protected RemoteWebDriver driver = null;

    private String driverLocation = null;
    private DesiredCapabilities capabilities;

    private static final String fileName = "C:\\WINDOWS\\system32\\drivers\\etc\\hosts";

    public String getConfigProperty(String environment, String key)
            throws Exception {
        return ConfigurationInstance.getInstance(environment).getConfigProperty(key);
    }


    private void createChromeDriver(String env) throws Exception {
        if (Boolean.valueOf(getConfigProperty(env, "browser.remote.driver"))) {
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--incognito");
            //options.addArguments("--disable-bundled-ppapi-flash");
            capabilities = DesiredCapabilities.chrome();
            capabilities.setCapability(ChromeOptions.CAPABILITY, options);
            capabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
            capabilities.setCapability(CapabilityType.SUPPORTS_APPLICATION_CACHE, false);
            capabilities.setCapability(CapabilityType.PAGE_LOAD_STRATEGY, "none");
            capabilities.setCapability(CapabilityType.SUPPORTS_FINDING_BY_CSS, true);
            capabilities.setCapability(CapabilityType.SUPPORTS_LOCATION_CONTEXT, true);
            capabilities.setCapability(CapabilityType.SUPPORTS_NETWORK_CONNECTION, true);
            capabilities.setCapability(CapabilityType.SUPPORTS_WEB_STORAGE, true);
            capabilities.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, false);
            capabilities.setCapability(CapabilityType.SUPPORTS_ALERTS, false);
            capabilities.setJavascriptEnabled(false);
            setWebDriver(new RemoteWebDriver(new URL("http://10.0.0.252:4444/wd/hub"), capabilities));
        } else {
            driverLocation = getConfigProperty(env, "chrome.driver.location.windows");
            System.setProperty("webdriver.chrome.driver", driverLocation);
            capabilities = DesiredCapabilities.chrome();
            ChromeOptions options = new ChromeOptions();
            //options.addArguments("--disable-bundled-ppapi-flash");
            //options.addArguments("-incognito");
            capabilities.setCapability(ChromeOptions.CAPABILITY, options);
            driver = new ChromeDriver(capabilities);
            driver.manage().window().maximize();
            setWebDriver(driver);
        }
    }

    private void createFirefoxDriver(String environment)
            throws Exception {
        driverLocation = getConfigProperty(environment, "ff.driver.location.windows");
        System.setProperty("webdriver.gecko.driver", driverLocation);

        if (Boolean.valueOf(getConfigProperty(environment, "browser.remote.driver"))) {
            FirefoxProfile firefoxProfile = new FirefoxProfile();

            firefoxProfile.setPreference("print.always_print_silent", true);
            firefoxProfile.setPreference("print.show print progress", false);

            capabilities = DesiredCapabilities.firefox();
            capabilities.setBrowserName("firefox");
            capabilities.setCapability("marionette", true);
            capabilities.setCapability("firefox_profile", firefoxProfile.toJson());
            capabilities.setJavascriptEnabled(true);

            setWebDriver(new RemoteWebDriver(new URL("http://10.0.0.252:4444/wd/hub"), capabilities));
        } else {
            driverLocation = getConfigProperty(environment, "ff.driver.location.windows");
            System.setProperty("webdriver.gecko.driver", driverLocation);
            FirefoxProfile firefoxProfile = new FirefoxProfile();
            firefoxProfile.setPreference("browser.privatebrowsing.autostart", true);
            capabilities = DesiredCapabilities.firefox();
            capabilities.setCapability("marionette", true);
            capabilities.setCapability(FirefoxDriver.PROFILE, firefoxProfile);
            driver = new FirefoxDriver(capabilities);
            setWebDriver(driver);
        }
    }

    private void buildWebDriver(String env, String browser) throws Exception {
        switch (browser) {
            case "chrome":
                createChromeDriver(env);
                break;
            case "firefox":
                createFirefoxDriver(env);
                break;
            default:
                createChromeDriver(env);
                break;
        }
        getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        getDriver().manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        getDriver().manage().deleteAllCookies();
    }

    public void setUp(String ipAddress, String browser, String environment) throws Exception {
        buildWebDriver(environment, browser);
        writeHostsFile(ipAddress);
        readHostsFile();
        executeCmdCommand("cmd /c ipconfig /flushdns");
        getDriver().get(getConfigProperty(environment, "url"));
    }

    private void writeHostsFile(String ipAddress) {
        System.out.println("Dosyaya yazilacak ip: " + ipAddress);
        // dosyaya yazdırma
        String pattern = ipAddress;
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(fileName));
            out.write(pattern);
            out.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void readHostsFile() throws IOException {
        FileReader fileReader = new FileReader(fileName);
        String line;
        BufferedReader br = new BufferedReader(fileReader);

        while ((line = br.readLine()) != null) {
            System.out.println("Host dosyasından okunan deger: " + line);
        }
    }

    private void executeCmdCommand(String command) throws IOException {
        // windows cmd komutu çalıştırma
        Process process = Runtime.getRuntime().exec(command);
        BufferedReader stdInput = new BufferedReader(new
                InputStreamReader(process.getInputStream()));

        // calısan komutun outputunu çekme
        System.out.println("Output of the command:");
        String s = null;
        while ((s = stdInput.readLine()) != null) {
            System.out.println(s);
        }
    }

    public void tearDown() {
        getDriver().quit();
    }
}

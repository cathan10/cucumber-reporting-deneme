package utils;

import org.openqa.selenium.remote.RemoteWebDriver;

/*
 * Created by cagatay.han on 2.06.2017.
 */
public class DriverManager extends ThreadLocal{
    private static ThreadLocal<String> driverError = new ThreadLocal<String>();

    private static ThreadLocal<RemoteWebDriver> webDriver = new ThreadLocal<RemoteWebDriver>();

    public static RemoteWebDriver getDriver() {
        return webDriver.get();
    }

    public static void setWebDriver(RemoteWebDriver driver) {
        webDriver.set(driver);
    }

    public static String getDriverError() {
        return driverError.get();
    }

    public static void setDriverError(String error) {
        driverError.set(error);
    }
}

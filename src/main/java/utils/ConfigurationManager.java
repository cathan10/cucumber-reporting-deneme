package utils;

/*
 * Created by cagatay.han on 2.06.2017.
 */
public class ConfigurationManager {

    public static String getConfigProperty(String environment,String key)
            throws Exception
    {
        return ConfigurationInstance.getInstance(environment).getConfigProperty(key);
    }
}
